"""PPW_kun URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/2.1/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.contrib import admin
from django.urls import include, path
from Lab4.views import home as page1
from Lab4.views import profil as page2
from Lab4.views import portfolio as page3
from Lab4.views import guest as page4
from django.urls import path, include
from django.urls import re_path,include

urlpatterns = [
    path('admin/', admin.site.urls),
    re_path(r'^$', page1,name="home"),
    re_path('home', page1,name="home"),
    re_path('profil', page2, name="profil"),
    re_path('portfolio', page3, name="portfolio"),
    re_path('guest', page4, name="guest")

]
