from django.shortcuts import render

# Create your views here.
from django.http import HttpResponse


def home(request):
		
	return render(request,'Story 3.html')

def profil(request):
		
	return render(request,'Profil.html')

def portfolio(request):
		
	return render(request,'Portfolio.html')

def guest(request):
		
	return render(request,'GuestBook.html')

